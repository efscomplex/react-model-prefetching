import React from 'react'
import Layout from './Layout'
import Main from './Main'
import { withApis } from 'services/providers/ApiService'

const App: React.FC = () => {
	return (
		<Layout>
			<header>header</header>
			<Main />
			<footer>footer</footer>
		</Layout>
	)
}
export default withApis(App)
