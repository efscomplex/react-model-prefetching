import React from 'react'
import styled from 'styled-components'

const Layout: React.FC = ({ children }) => {
	return <AppLayout>{children}</AppLayout>
}

const AppLayout = styled('div')`
	min-height: 100vh;
	display: grid;
	grid-template-rows: min-content 1fr min-content;

	background-color: #333;
	color: white;
`
export default Layout
