import Section from 'components/shared/Section'
import useQuery from 'hooks/useQuery'
import React from 'react'
import Spinner from 'components/shared/spinner/Spinner'

const Main: React.FC = () => {
	const { data, error, isLoading } = useQuery('comments/1', 'jph')

	if (error) return null
	return (
		<main>
			<Section>
				{isLoading ? <Spinner /> : JSON.stringify(data, null, 2)}
			</Section>
		</main>
	)
}
export default Main
