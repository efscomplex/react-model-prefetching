import React from 'react'

const Section: React.FC = ({ children }) => {
	return <section>{children}</section>
}
export default Section
