import React, { useState } from 'react'
import { Api } from 'services/apis/Api'
import { useApis, ApiContext, ApiName } from 'services/providers/ApiService'

const useQuery = (endpoint: string, apiName: ApiName) => {
	const apis: ApiContext = useApis()
	const api = apis[apiName]

	const [data, setData] = useState()
	const [isLoading, setIsLoading] = useState(true)
	const [error, setError] = useState()

	const [query] = useState(() => api.query(endpoint)) // prefetching

	React.useEffect(() => {
		query()
			.then(setData)
			.catch(setError)
			.finally(() => setIsLoading(false))
	}, [])
	return { data, isLoading, error }
}

export default useQuery
