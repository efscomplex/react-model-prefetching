export class Api {
	query: (enpoint: string) => any

	constructor(baseUrl: string) {
		this.query = (endpoint) => () =>
			fetch(`${baseUrl}/${endpoint}`, {
				method: 'get',
				headers: {
					'Content-Type': 'application/json'
				}
			}).then((resp) => resp.json())
	}
}
