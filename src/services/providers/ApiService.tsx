import { useContext, createContext } from 'react'
import { Api } from 'services/apis/Api'
import React from 'react'

export type ApiName = 'jph' | 'another'
export type ApiContext = {
	[key: string]: Api
}
const ApiServiceContext = createContext({} as ApiContext)

export const withApis = (Comp: React.FC) => {
	const jph = new Api('https://jsonplaceholder.typicode.com')

	return () => (
		<ApiServiceContext.Provider value={{ jph }}>
			<Comp />
		</ApiServiceContext.Provider>
	)
}

export const useApis = () => useContext(ApiServiceContext)
